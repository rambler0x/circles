package me.mradul.circles;

/**
 * Created by Mradul on 8/22/2017.
 */

public class User {
    private String collegeName;
    private String passoutYear;
    private String interests;
    private String projects;
    private String skills;
    private String name;
    private String email;
    private String imageUrl;

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getPassoutYear() {
        return passoutYear;
    }

    public void setPassoutYear(String passoutYear) {
        this.passoutYear = passoutYear;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public String getProjects() {
        return projects;
    }

    public void setProjects(String projects) {
        this.projects = projects;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public User(){

    }

    public User(String collegeName, String passoutYear, String interests, String projects,
                String skills, String name, String email, String imageUrl){
        this.collegeName = collegeName;
        this.passoutYear = passoutYear;
        this.interests = interests;
        this.projects = projects;
        this.skills = skills;
        this.name = name;
        this.email = email;
        this.imageUrl = imageUrl;
    }
}
