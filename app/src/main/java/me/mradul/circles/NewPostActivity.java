package me.mradul.circles;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PermissionGroupInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

public class NewPostActivity extends AppCompatActivity {

    private Spinner mPrivacySpinner;
    private TextView mFilesTextView;
    private TextView mImagesTextView;
    private LinearLayout mImagePreviewLinearLayout;
    private LinearLayout mDocPreviewLinearLayout;
    private EditText mPostTextEditText;
    private StorageReference mStorageReference;
    private String collegeName;
    private String passOutYear;
    private static User userLocal;

    private ArrayList<String> photoPaths;
    private ArrayList<String> docPaths;
    private FirebaseDatabase mFirebaseDatabase;
    private Post sPost = new Post();
    private int RC_READ_MEDIA = 8769;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);
        initializeViews();
        setListeners();
        //Firebase
        userLocal = new User();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference mRef = mFirebaseDatabase.getReference("/user/"+FirebaseAuth.getInstance()
                .getCurrentUser().getUid());
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userLocal = dataSnapshot.getValue(User.class);
                if(userLocal!=null){
                    collegeName = userLocal.getCollegeName();
                    passOutYear = userLocal.getPassoutYear();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mStorageReference = FirebaseStorage.getInstance().getReference();
    }

    private void startPhotoPicker(){
        FilePickerBuilder.getInstance().setMaxCount(10)
                .setSelectedFiles(photoPaths)
                .setActivityTheme(R.style.AppTheme)
                .pickPhoto(NewPostActivity.this);
    }

    private void setListeners() {
        mImagesTextView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
                    startPhotoPicker();
                }
                else {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        startPhotoPicker();
                    } else {
                        requestMediaPermission();
                    }
                }
            }
        });



        mFilesTextView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
                    startFilePicker();
                }
                else if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {
                    startFilePicker();
                } else {
                    requestMediaPermission();
                }
            }
        });

    }

    private void startFilePicker(){
        FilePickerBuilder.getInstance().setMaxCount(10)
                .setSelectedFiles(docPaths)
                .setActivityTheme(R.style.AppTheme)
                .pickFile(NewPostActivity.this);
    }

    private void requestMediaPermission() {
        ActivityCompat.requestPermissions(NewPostActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, RC_READ_MEDIA);
    }

    private void initializeViews() {
        mPrivacySpinner = (Spinner) findViewById(R.id.privacySpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.privacy_choices, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mPrivacySpinner.setAdapter(adapter);

        mImagesTextView = (TextView) findViewById(R.id.add_images_textView);
        mFilesTextView = (TextView) findViewById(R.id.add_files_textView);

        mDocPreviewLinearLayout = (LinearLayout) findViewById(R.id.linear_layout_selected_files);
        mImagePreviewLinearLayout = (LinearLayout) findViewById(R.id.linear_layout_selected_images);

        mPostTextEditText = (EditText) findViewById(R.id.post_text_edit_text);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_new_post, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if(resultCode== Activity.RESULT_OK && data!=null)
                {
                    photoPaths = new ArrayList<>();
                    photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                    sPost.setHasPhoto(true);
                    for(String path: photoPaths){
                        File imageFile = new File(path);
                        if(imageFile.exists()){
                            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                            ImageView imageView = new ImageView(NewPostActivity.this);
                            imageView.setImageBitmap(bitmap);
                            imageView.setMaxHeight(100);
                            imageView.setMaxHeight(100);
                            mImagePreviewLinearLayout.addView(imageView, 200, 200);

                        }
                    }
                }
                break;
            case FilePickerConst.REQUEST_CODE_DOC:
                if(resultCode== Activity.RESULT_OK && data!=null)
                {
                    docPaths = new ArrayList<>();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));
                    sPost.setHasDoc(true);
                    for(String path: docPaths){
                        File docFile = new File(path);
                        if(docFile.exists()){
                            TextView textView = new TextView(NewPostActivity.this);
                            textView.setText(docFile.getName());
                            textView.setTextSize(getResources().getDimension(R.dimen.file_name));
                            mDocPreviewLinearLayout.addView(textView);
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId){
            case R.id.post_button:
                DatabaseReference mRef = mFirebaseDatabase.getReference();
                if(collegeName==null||passOutYear==null){
                    Toast.makeText(NewPostActivity.this, "You have to set your college name and passout year" +
                            " in profile section before making a post", Toast.LENGTH_SHORT).show();
                    return false;
                }
                switch(mPrivacySpinner.getSelectedItem().toString()){
                    case "Public":
                        mRef = mFirebaseDatabase.getReference("/posts/public");
                        break;
                    case "College":
                        mRef = mFirebaseDatabase.getReference("/posts/"+collegeName+"/public");
                        break;

                    case "Batchmates":
                        mRef = mFirebaseDatabase.getReference("/posts/"+collegeName+"/"+passOutYear);
                        break;

                }


                String key = mRef.push().getKey();
                sPost.setKey(key);
                sPost.setuId(FirebaseAuth.getInstance().getCurrentUser().getUid());
                sPost.setText(mPostTextEditText.getText().toString());
                sPost.setUserName(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                if(sPost.hasPhoto()){
                    for(String path: photoPaths){
                        File file = new File(path);
                        if(file.exists()){
                            try {
                                FileInputStream fis = new FileInputStream(file);
                                StorageReference imageRef = mStorageReference.child("image/"+key+"/"+(sPost.getNumImages()+1));
                                UploadTask uploadTask = imageRef.putStream(fis);
                                sPost.setNumImages(sPost.getNumImages()+1);
                                uploadTask.addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(NewPostActivity.this, "Can't upload images",Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                });

                            } catch (FileNotFoundException e) {

                            }
                        }
                    }
                }
                if(sPost.hasDoc()){
                    for(String path: docPaths){
                        final File file = new File(path);
                        if(file.exists())
                            try {
                                FileInputStream fis = new FileInputStream(file);
                                StorageReference docRef = mStorageReference.child("doc/"+key+"/"+(sPost.getNumFiles()+1));
                                sPost.setNumFiles(sPost.getNumFiles()+1);
                                UploadTask uploadTask = docRef.putStream(fis);
                                uploadTask.addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(NewPostActivity.this, "Can't upload document", Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                });

                            } catch (FileNotFoundException e) {

                            }
                    }
                }
                mRef.child(key).setValue(sPost);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
