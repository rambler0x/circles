package me.mradul.circles;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private FirebaseAuth mAuth;
    private GoogleApiClient mApiClient;
    private int RC_SIGN_IN = 35611;
    public static final int RC_MAIN_ACTIVITY = 5613;
    private ProgressBar mLoginProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Views
        mLoginProgress = (ProgressBar) findViewById(R.id.login_progress_bar);

        //Setup Listener
        findViewById(R.id.sign_in_button).setOnClickListener(this);

        //Configure GoogleSiginOptions
        GoogleSignInOptions gso =new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(this.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        //Configure Google ApiClient
        mApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //Get Firebase Instance
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser!=null){
            startMainActivity();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Result returned from launching the intent GoogleSigninApi.getIntent
        if(requestCode==RC_SIGN_IN){
            Log.d(this.getClass().getName(), "Returned to Activity");
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d(this.getClass().getCanonicalName(), result.getStatus().toString());
            if(result.isSuccess()){
                Log.d(this.getClass().getCanonicalName(), "Sigin successful");
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            }
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount account) {
        Log.d(this.getClass().getCanonicalName(), "firebaseAuthWithAccount" + account.getId());
        showLoginProgress();
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.d(this.getClass().getName(), "SigninWithCredential:isSuccess");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Bundle bundle = new Bundle();
                            startMainActivity();
                        } else {
                            Log.w(this.getClass().getName(), "SigninWithCredential Failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication Failed", Toast.LENGTH_SHORT);
                        }
                        hideLoginProgress();
                    }
                });
    }

    private void startMainActivity() {
        hideLoginProgress();
        Intent intent = new Intent(this, MainActivity.class);
        startActivityForResult(intent,RC_MAIN_ACTIVITY);
    }

    @Override
    public void onClick(View v) {
        signIn();
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(this.getClass().getCanonicalName(), "Connection Failed "+connectionResult);
        Toast.makeText(this, "Google Play Services Error", Toast.LENGTH_SHORT).show();
    }

    private void showLoginProgress(){
        mLoginProgress.setVisibility(View.VISIBLE);
    }

    private void hideLoginProgress(){
        mLoginProgress.setVisibility(View.INVISIBLE);
    }
}
