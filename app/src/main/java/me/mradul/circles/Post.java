package me.mradul.circles;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Mradul on 8/22/2017.
 */

public class Post {


    private String uId;
    private String text;
    private String key;

    public int getNumImages() {
        return numImages;
    }

    public void setNumImages(int numImages) {
        this.numImages = numImages;
    }

    public int getNumFiles() {
        return numFiles;
    }

    public void setNumFiles(int numFiles) {
        this.numFiles = numFiles;
    }

    private int numImages = 0;
    private int numFiles = 0;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private String userName;

    public boolean isHasDoc() {
        return hasDoc;
    }

    public boolean isHasPhoto() {
        return hasPhoto;
    }

    private boolean hasPhoto;
    private boolean hasDoc;

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }




    public boolean hasPhoto(){
        return hasPhoto;
    }

    public void setHasPhoto(boolean hasPhoto){
        this.hasPhoto = hasPhoto;
    }

    public  boolean hasDoc(){
        return hasDoc;
    }

    public void setHasDoc(boolean hasDoc){
        this.hasDoc = hasDoc;
    }

    public Post(){
    }

    public Post(String uId, String key, String text, boolean hasPhoto, boolean hasDoc, String userName, int numFiles,
                int numImages){
        this.text = text;
        this.key = key;
        this.uId = uId;
        this.hasPhoto = hasPhoto;
        this.hasDoc = hasDoc;
        this.userName = userName;
        this.numFiles = numFiles;
        this.numImages = numImages;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
