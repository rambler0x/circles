package me.mradul.circles;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProfileActivity extends AppCompatActivity {

    private ImageView mProfileImageView;
    private EditText mCollegeEditText;
    private EditText mPassoutYearEditText;
    private EditText mUserSkillsEditText;
    private EditText mInterestsEditText;
    private EditText mProjectsEditText;
    private TextView mUsernameTextView;
    private TextView mUserEmailTextView;
    private FloatingActionButton mSaveButton;

    private FirebaseUser mUser;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mRef;

    private static User userProfile;

    private int updateCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initializeViews();
        //Firebase
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance();

        //Profile data load
        if(mProfileImageView!=null){
            Glide.with(this).load(mUser.getPhotoUrl())
                    .into(mProfileImageView);
        }
        mUsernameTextView.setText(mUser.getDisplayName());
        mUserEmailTextView.setText(mUser.getEmail());

        userProfile = new User();

        loadData();
        setListeners();
    }

    private void setListeners() {
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userProfile = new User();
                userProfile.setCollegeName(mCollegeEditText.getText().toString());
                userProfile.setProjects(mProjectsEditText.getText().toString());
                userProfile.setInterests(mInterestsEditText.getText().toString());
                userProfile.setPassoutYear(mPassoutYearEditText.getText().toString());
                userProfile.setSkills(mUserSkillsEditText.getText().toString());
                userProfile.setName(mUser.getDisplayName());
                userProfile.setEmail(mUser.getEmail());
                userProfile.setImageUrl(mUser.getPhotoUrl().toString());
                mRef.setValue(userProfile);
            }
        });
    }

    private void loadData() {
        mRef = mDatabase.getReference("/user/"+mUser.getUid());
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userProfile = dataSnapshot.getValue(User.class);
                if(userProfile!=null){
                    mCollegeEditText.setText(userProfile.getCollegeName());
                    mProjectsEditText.setText(userProfile.getProjects());
                    mInterestsEditText.setText(userProfile.getInterests());
                    mPassoutYearEditText.setText(userProfile.getPassoutYear());
                    mUserSkillsEditText.setText(userProfile.getSkills());
                    if(updateCount!=0){
                        Toast.makeText(ProfileActivity.this, "Your profile has been updated!", Toast.LENGTH_SHORT)
                                .show();
                    }
                    updateCount++;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void initializeViews() {
        mProfileImageView = (ImageView) findViewById(R.id.user_image);
        mCollegeEditText = (EditText) findViewById(R.id.user_college);
        mPassoutYearEditText = (EditText) findViewById(R.id.user_passout_year);
        mUserSkillsEditText = (EditText) findViewById(R.id.user_skills);
        mInterestsEditText = (EditText) findViewById(R.id.user_interests);
        mProjectsEditText = (EditText) findViewById(R.id.user_projects);
        mUsernameTextView = (TextView) findViewById(R.id.user_name);
        mUserEmailTextView = (TextView) findViewById(R.id.user_email);
        mSaveButton = (FloatingActionButton) findViewById(R.id.save_button);
    }
}
