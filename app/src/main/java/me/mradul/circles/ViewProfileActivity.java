package me.mradul.circles;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ViewProfileActivity extends AppCompatActivity {

    private String uid;
    private ImageView mUserProfileImageView;
    private TextView mUserNameTextView;
    private TextView mUserEmailTextView;
    private TextView mCollegeNameTextView;
    private TextView mPassoutYearTextView;
    private TextView mSkillsTextView;
    private TextView mInterestsTextView;
    private TextView mProjectsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);
        uid = getIntent().getStringExtra("uid");

        initializeViews();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("/user/"+uid);
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if(user!=null){
                    Glide.with(ViewProfileActivity.this)
                            .load(user.getImageUrl())
                            .into(mUserProfileImageView);
                    mUserNameTextView.setText(user.getName());
                    mUserEmailTextView.setText(user.getEmail());
                    mCollegeNameTextView.setText(user.getCollegeName());
                    mPassoutYearTextView.setText(user.getPassoutYear());
                    mSkillsTextView.setText(user.getSkills());
                    mInterestsTextView.setText(user.getInterests());
                    mProjectsTextView.setText(user.getProjects());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void initializeViews() {
        mUserProfileImageView = (ImageView) findViewById(R.id.user_image);
        mUserNameTextView = (TextView) findViewById(R.id.user_name);
        mUserEmailTextView = (TextView) findViewById(R.id.user_email);
        mCollegeNameTextView = (TextView) findViewById(R.id.user_college);
        mPassoutYearTextView = (TextView) findViewById(R.id.user_passout_year);
        mSkillsTextView = (TextView) findViewById(R.id.user_skills);
        mInterestsTextView = (TextView) findViewById(R.id.user_interests);
        mProjectsTextView = (TextView) findViewById(R.id.user_projects);
    }
}
