package me.mradul.circles;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener{

    private FirebaseAuth mAuth;
    private GoogleApiClient mApiClient;
    private FirebaseUser mUser;
    private ImageView mUserImageView;
    private TextView mUserNameTextView;
    private TextView mUserEmailTextView;
    private Toast mExitToast;
    private int backPressed = 0;

    private TextView mNewPostTextView;
    private RecyclerView mNewsFeedRecyclerView;
    private List<Post> mNewsFeedPosts = new ArrayList<>();
    private NewsFeedAdapter mNewsFeedAdapter;
    private ProgressBar mProgressBar;
    private LinearLayout mProgressBarLinearLayout;
    private static User userLocal;

    private static String collegeName;
    private static String passOutYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Views
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //Initialize Views
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        mUserImageView = (ImageView) header.findViewById(R.id.user_image);
        mUserNameTextView = (TextView) header.findViewById(R.id.user_name);
        mUserEmailTextView = (TextView) header.findViewById(R.id.user_email);
        mNewPostTextView = (TextView) findViewById(R.id.new_post_text_view);
        mNewsFeedRecyclerView = (RecyclerView) findViewById(R.id.news_feed_recycler_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mProgressBarLinearLayout = (LinearLayout) findViewById(R.id.progressBarLinearLayout) ;

        //Firebase
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        //Load user profile
        mUserEmailTextView.setText(mUser.getEmail());
        mUserNameTextView.setText(mUser.getDisplayName());
        if(mUserImageView!=null)
        Glide.with(this).load(mUser.getPhotoUrl())
                .thumbnail(1f)
                .into(mUserImageView);
        mNewPostTextView.setText("Hello, "+mUser.getDisplayName()+"! Share something with your mates!");
        //Google Signin
        //Configure GoogleSiginOptions
        GoogleSignInOptions gso =new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(this.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        //Configure Google ApiClient
        mApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        setListeners();

        userLocal = new User();
        //
        DatabaseReference ref = FirebaseDatabase.getInstance().
                getReference("/user/"+mUser.getUid());
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userLocal = dataSnapshot.getValue(User.class);
                if(userLocal!=null){
                    collegeName = userLocal.getCollegeName();
                    passOutYear = userLocal.getPassoutYear();
                }
                setupPrivateNewsFeed();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        //Feed
        mNewsFeedAdapter = new NewsFeedAdapter(this, mNewsFeedPosts);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        mNewsFeedRecyclerView.setLayoutManager(layoutManager);
        mNewsFeedRecyclerView.setItemViewCacheSize(25);
        mNewsFeedRecyclerView.setAdapter(mNewsFeedAdapter);
        setupPublicNewsFeed();
    }

    private void setupPrivateNewsFeed() {
        //College
        if(collegeName!=null && !TextUtils.isEmpty(collegeName)){
            DatabaseReference collegeRef = FirebaseDatabase.getInstance().getReference("/posts/"
                    +collegeName+"/public");
            collegeRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Post post = dataSnapshot.getValue(Post.class);
                    if(post!=null){
                        mNewsFeedAdapter.addPost(post);
                        mProgressBarLinearLayout.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        //Batch
        if(passOutYear!=null && !TextUtils.isEmpty(passOutYear)){
            DatabaseReference batchRef = FirebaseDatabase.getInstance().getReference("/posts/"+collegeName
                    +"/"+passOutYear);
            batchRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Post post = dataSnapshot.getValue(Post.class);
                    if(post!=null){
                        mNewsFeedAdapter.addPost(post);
                        mProgressBarLinearLayout.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });;
        }
    }

    private void setupPublicNewsFeed() {

        //Public Posts
        DatabaseReference pubRef = FirebaseDatabase.getInstance().getReference("/posts/public");
        pubRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Post post = dataSnapshot.getValue(Post.class);
                if(post!=null){
                    mNewsFeedAdapter.addPost(post);
                    mProgressBarLinearLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void setListeners() {
        mNewPostTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewPostActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(backPressed >=1){
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mExitToast.cancel();
                startActivity(intent);
                finish();
            } else {
                backPressed++;
                mExitToast = Toast.makeText(this, "Click once more to exit", Toast.LENGTH_SHORT);
                mExitToast.show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_logout:
                mAuth.signOut();
                Auth.GoogleSignInApi.signOut(mApiClient);
                finish();
                break;
        }
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.user_profile) {
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //to implement
    }
}
