package me.mradul.circles;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.LinkAddress;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import java.util.List;

/**
 * Created by Mradul on 8/23/2017.
 */

public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.ViewHolder> {
    private List<Post> mPostList;
    private static Context sContext;
    RequestOptions mRequestOptions;

    NewsFeedAdapter(Context context, List<Post> postList){
        sContext = context;
        mPostList = postList;
        notifyDataSetChanged();
        mRequestOptions = new RequestOptions();
        mRequestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
    }

    public void addPost(Post post){
        mPostList.add(0, post);
        notifyItemInserted(0);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.newsfeed_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Post post = mPostList.get(position);
        holder.mPostedByTextView.setText(post.getUserName());
        holder.mPostedByTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(sContext, ViewProfileActivity.class);
                intent.putExtra("uid", post.getuId());
                sContext.startActivity(intent);
            }
        });
        holder.mPostTextTextView.setText(post.getText());
        if(post.hasPhoto()==true){
            holder.mPostImagesLinearLayout.removeAllViews();
            holder.mPostImagesLinearLayout.invalidate();
            for(int i=0;i<post.getNumImages();i++){
                StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                        .child("image/"+post.getKey()+"/"+(i+1));
                storageReference.getDownloadUrl().addOnSuccessListener(
                        new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                ImageView imageView = new ImageView(sContext);
                                holder.mPostImagesLinearLayout.addView(imageView);
                                Glide.with(sContext)
                                        .setDefaultRequestOptions(mRequestOptions)
                                        .load(uri)
                                        .into(imageView);
                            }
                        }
                );
            }
        } else {
            for(int i=0; i<holder.mPostImagesLinearLayout.getChildCount();i++){
                Glide.with(sContext).clear(holder.mPostImagesLinearLayout.getChildAt(i));
                holder.mPostImagesLinearLayout.removeViewAt(i);
                holder.mPostImagesLinearLayout.invalidate();
            }
        }
        if(post.hasDoc()==true){
            holder.mPostFilesLinearLayout.removeAllViews();
            holder.mPostFilesLinearLayout.invalidate();
            for(int i=0;i<post.getNumFiles();i++){
                StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                        .child("doc/"+post.getKey()+"/"+(i+1));
                storageReference.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                    @Override
                    public void onSuccess(final StorageMetadata storageMetadata) {
                        TextView textView = new TextView(sContext);
                        textView.setText(storageMetadata.getName()+storageMetadata.getContentType());
                        textView.setTextColor(Color.BLUE);
                        textView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Uri uri = Uri.parse(storageMetadata.getDownloadUrl().toString());
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                sContext.startActivity(intent);
                            }
                        });
                        holder.mPostFilesLinearLayout.addView(textView);
                    }
                });
            }
        } else {
            holder.mPostFilesLinearLayout.removeAllViews();
            holder.mPostFilesLinearLayout.invalidate();
        }

    }

    @Override
    public int getItemCount() {
        return mPostList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mPostedByTextView;
        TextView mPostTextTextView;
        LinearLayout mPostImagesLinearLayout;
        LinearLayout mPostFilesLinearLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            mPostedByTextView = (TextView) itemView.findViewById(R.id.posted_by_text_view);
            mPostTextTextView = (TextView) itemView.findViewById(R.id.post_text_text_view);
            mPostImagesLinearLayout = (LinearLayout) itemView.findViewById(R.id.post_images_linear_layout);
            mPostFilesLinearLayout = (LinearLayout) itemView.findViewById(R.id.post_files_linear_layout);
        }
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        for(int i=0; i<holder.mPostImagesLinearLayout.getChildCount(); i++){
            Glide.with(sContext).clear(holder.mPostImagesLinearLayout.getChildAt(i));
            holder.mPostImagesLinearLayout.removeViewAt(i);
            holder.mPostImagesLinearLayout.invalidate();
        }

        if(holder.mPostFilesLinearLayout.getChildCount()>0){
            holder.mPostFilesLinearLayout.removeAllViews();
            holder.mPostFilesLinearLayout.invalidate();
        }

    }
}
